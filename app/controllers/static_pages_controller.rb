class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end
  
  def statspage
  end

  def about
  end

  def contact
  end
  
  def newgames
  end 
  
  def game
  end
  
  def one
    aberdeen = Scot.find_by(team: 'Aberdeen')
    @wins = aberdeen.wins
    @losses = aberdeen.losses
  end
  
  def two
    celtic = Scot.find_by(team: 'Celtic')
    @wins = celtic.wins
    @losses = celtic.losses
  end
  
  def three
    dundee = Scot.find_by(team: 'Dundee')
    @wins = dundee.wins
    @losses = dundee.losses
  end
  
  def four
    dundee_united = Scot.find_by(team: 'Dundee United')
    @wins = dundee_united.wins
    @losses = dundee_united.losses
  end
  
  def five
    hamilton_academics = Scot.find_by(team: 'Hamilton Academics')
    @wins = hamilton_academics.wins
    @losses = hamilton_academics.losses
  end
  
  def six
    hearts = Scot.find_by(team: 'Hearts')
    @wins = hearts.wins 
    @losses = hearts.losses
  end 
  
  def seven
    inverness_ct = Scot.find_by(team: 'Inverness CT')
    @wins = inverness_ct.wins
    @losses = inverness_ct.losses
  end
  
  def eight
     kilmarnock = Scot.find_by(team: 'Kilmarnock')
    @wins = kilmarnock.wins
    @losses = kilmarnock.losses
  end
  
  def nine
    motherwell = Scot.find_by(team: 'Motherwell')
    @wins = motherwell.wins
    @losses = motherwell.losses
  end
  
  def ten
    partick_thistle = Scot.find_by(team: 'Partick Thistle')
    @wins = partick_thistle.wins
    @losses = partick_thistle.losses
  end
  
  def eleven
    ross_county = Scot.find_by(team: 'Ross County')
    @wins = ross_county.wins
    @losses = ross_county.losses
  end
  
  def twelve
    st_johnstone = Scot.find_by(team: 'St Johnstone')
    @wins = st_johnstone.wins
    @losses = st_johnsone.losses
  end
  
end