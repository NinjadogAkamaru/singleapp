class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :team
      t.integer :wins
      t.integer :losses

      t.timestamps null: false
    end
  end
end
