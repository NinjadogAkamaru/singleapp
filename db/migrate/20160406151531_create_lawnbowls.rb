class CreateLawnbowls < ActiveRecord::Migration
  def change
    create_table :lawnbowls do |t|
      t.string :name
      t.string :address
      t.string :owner
      t.decimal :bng_x
      t.decimal :bng_y

      t.timestamps null: false
    end
  end
end
